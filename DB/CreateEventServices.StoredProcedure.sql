

/****** Object:  StoredProcedure [dbo].[CreateEventServices]    Script Date: 12/17/2015 22:55:58 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Soumalya
-- Create date: 17-12-2015
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[CreateEventServices] 
	-- Add the parameters for the stored procedure here
	@customerId int,
	@services varchar(max)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	--DECLARE @str varchar(50)
 -- SET @str='John, Samantha, Bob, Tom'


  
  Create table #temp1
  ( id int,
  servicen int)
  
  insert into #temp1(id,servicen)
  SELECT @customerId, y.i.value('(./text())[1]', 'nvarchar(1000)')             
  FROM 
  ( 
    SELECT 
        n = CONVERT(XML, '<i>' 
            + REPLACE(@services, ',' , '</i><i>') 
            + '</i>')
  ) AS a 
  CROSS APPLY n.nodes('i') AS y(i)  
  
  INSERT INTO CustomerService
  SELECT ID,servicen FROM #temp1
  
  drop table #temp1
END

GO


