USE [EventManagement]
GO
/****** Object:  Table [dbo].[CustomerLocation]    Script Date: 12/15/2015 21:50:52 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_CustomerLocation_Customer]') AND parent_object_id = OBJECT_ID(N'[dbo].[CustomerLocation]'))
ALTER TABLE [dbo].[CustomerLocation] DROP CONSTRAINT [FK_CustomerLocation_Customer]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_CustomerLocation_LocationAddress]') AND parent_object_id = OBJECT_ID(N'[dbo].[CustomerLocation]'))
ALTER TABLE [dbo].[CustomerLocation] DROP CONSTRAINT [FK_CustomerLocation_LocationAddress]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_CustomerLocation_Customer]') AND parent_object_id = OBJECT_ID(N'[dbo].[CustomerLocation]'))
ALTER TABLE [dbo].[CustomerLocation] DROP CONSTRAINT [FK_CustomerLocation_Customer]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_CustomerLocation_LocationAddress]') AND parent_object_id = OBJECT_ID(N'[dbo].[CustomerLocation]'))
ALTER TABLE [dbo].[CustomerLocation] DROP CONSTRAINT [FK_CustomerLocation_LocationAddress]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CustomerLocation]') AND type in (N'U'))
DROP TABLE [dbo].[CustomerLocation]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CustomerLocation]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[CustomerLocation](
	[custLocID] [int] IDENTITY(1,1) NOT NULL,
	[locAddrID] [int] NOT NULL,
	[custID] [int] NOT NULL,
 CONSTRAINT [PK_CustomerLocation] PRIMARY KEY CLUSTERED 
(
	[custLocID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_CustomerLocation_Customer]') AND parent_object_id = OBJECT_ID(N'[dbo].[CustomerLocation]'))
ALTER TABLE [dbo].[CustomerLocation]  WITH CHECK ADD  CONSTRAINT [FK_CustomerLocation_Customer] FOREIGN KEY([custID])
REFERENCES [dbo].[Customer] ([custID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_CustomerLocation_Customer]') AND parent_object_id = OBJECT_ID(N'[dbo].[CustomerLocation]'))
ALTER TABLE [dbo].[CustomerLocation] CHECK CONSTRAINT [FK_CustomerLocation_Customer]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_CustomerLocation_LocationAddress]') AND parent_object_id = OBJECT_ID(N'[dbo].[CustomerLocation]'))
ALTER TABLE [dbo].[CustomerLocation]  WITH CHECK ADD  CONSTRAINT [FK_CustomerLocation_LocationAddress] FOREIGN KEY([locAddrID])
REFERENCES [dbo].[LocationAddress] ([locAddrID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_CustomerLocation_LocationAddress]') AND parent_object_id = OBJECT_ID(N'[dbo].[CustomerLocation]'))
ALTER TABLE [dbo].[CustomerLocation] CHECK CONSTRAINT [FK_CustomerLocation_LocationAddress]
GO
