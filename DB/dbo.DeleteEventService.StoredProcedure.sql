USE [EventManagement]
GO
/****** Object:  StoredProcedure [dbo].[DeleteEventService]    Script Date: 12/15/2015 21:50:52 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DeleteEventService]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[DeleteEventService]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DeleteEventService]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		<Author,,Abhijit Mazumdar>
-- Create date: <Create Date,, 15/12/2015>
-- Description:	<Description,,Deletes service attached with an event>
-- =============================================
CREATE PROCEDURE [dbo].[DeleteEventService] 
	-- Add the parameters for the stored procedure here
	@eventID int,
	@serviceID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	delete from eventservice where evntID = @eventID and svcID = @serviceID
END
' 
END
GO
