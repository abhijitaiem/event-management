USE [EventManagement]
GO
/****** Object:  Table [dbo].[EventService]    Script Date: 12/15/2015 21:50:49 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EventService]') AND type in (N'U'))
DROP TABLE [dbo].[EventService]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EventService]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[EventService](
	[evntID] [int] NOT NULL,
	[svcID] [int] NOT NULL
) ON [PRIMARY]
END
GO
INSERT [dbo].[EventService] ([evntID], [svcID]) VALUES (26, 15)
INSERT [dbo].[EventService] ([evntID], [svcID]) VALUES (24, 13)
INSERT [dbo].[EventService] ([evntID], [svcID]) VALUES (25, 14)
