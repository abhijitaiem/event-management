USE [EventManagement]
GO
/****** Object:  Table [dbo].[CustomerService]    Script Date: 12/15/2015 21:50:52 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_CustomerService_Customer]') AND parent_object_id = OBJECT_ID(N'[dbo].[CustomerService]'))
ALTER TABLE [dbo].[CustomerService] DROP CONSTRAINT [FK_CustomerService_Customer]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_CustomerService_Customer]') AND parent_object_id = OBJECT_ID(N'[dbo].[CustomerService]'))
ALTER TABLE [dbo].[CustomerService] DROP CONSTRAINT [FK_CustomerService_Customer]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CustomerService]') AND type in (N'U'))
DROP TABLE [dbo].[CustomerService]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CustomerService]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[CustomerService](
	[custID] [int] NOT NULL,
	[svcID] [int] NOT NULL,
 CONSTRAINT [PK_CustomerService_1] PRIMARY KEY CLUSTERED 
(
	[custID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_CustomerService_Customer]') AND parent_object_id = OBJECT_ID(N'[dbo].[CustomerService]'))
ALTER TABLE [dbo].[CustomerService]  WITH CHECK ADD  CONSTRAINT [FK_CustomerService_Customer] FOREIGN KEY([svcID])
REFERENCES [dbo].[Service] ([svcID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_CustomerService_Customer]') AND parent_object_id = OBJECT_ID(N'[dbo].[CustomerService]'))
ALTER TABLE [dbo].[CustomerService] CHECK CONSTRAINT [FK_CustomerService_Customer]
GO
