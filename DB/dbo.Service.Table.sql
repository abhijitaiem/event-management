USE [EventManagement]
GO
/****** Object:  Table [dbo].[Service]    Script Date: 12/15/2015 21:50:49 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Service]') AND type in (N'U'))
DROP TABLE [dbo].[Service]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Service]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Service](
	[svcID] [int] IDENTITY(1,1) NOT NULL,
	[svcName] [varchar](max) NOT NULL,
 CONSTRAINT [PK_Service] PRIMARY KEY CLUSTERED 
(
	[svcID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Service] ON
INSERT [dbo].[Service] ([svcID], [svcName]) VALUES (1, N'Test S1')
INSERT [dbo].[Service] ([svcID], [svcName]) VALUES (2, N'Test Service')
INSERT [dbo].[Service] ([svcID], [svcName]) VALUES (3, N'Test Service')
INSERT [dbo].[Service] ([svcID], [svcName]) VALUES (4, N'Test Service')
INSERT [dbo].[Service] ([svcID], [svcName]) VALUES (5, N'Test Service')
INSERT [dbo].[Service] ([svcID], [svcName]) VALUES (6, N'Test Service')
INSERT [dbo].[Service] ([svcID], [svcName]) VALUES (7, N'Test Service 007')
INSERT [dbo].[Service] ([svcID], [svcName]) VALUES (8, N'Test Service 123')
INSERT [dbo].[Service] ([svcID], [svcName]) VALUES (9, N'Test Service 007')
INSERT [dbo].[Service] ([svcID], [svcName]) VALUES (10, N'Test Service 007')
INSERT [dbo].[Service] ([svcID], [svcName]) VALUES (11, N'Test Service 007')
INSERT [dbo].[Service] ([svcID], [svcName]) VALUES (12, N'Test Service 007')
INSERT [dbo].[Service] ([svcID], [svcName]) VALUES (13, N'Test Service')
INSERT [dbo].[Service] ([svcID], [svcName]) VALUES (14, N'Test Service 123')
INSERT [dbo].[Service] ([svcID], [svcName]) VALUES (15, N'Test Service 007')
SET IDENTITY_INSERT [dbo].[Service] OFF
