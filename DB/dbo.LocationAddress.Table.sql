USE [EventManagement]
GO
/****** Object:  Table [dbo].[LocationAddress]    Script Date: 12/15/2015 21:50:52 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_LocationAddress_Location]') AND parent_object_id = OBJECT_ID(N'[dbo].[LocationAddress]'))
ALTER TABLE [dbo].[LocationAddress] DROP CONSTRAINT [FK_LocationAddress_Location]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_LocationAddress_Location]') AND parent_object_id = OBJECT_ID(N'[dbo].[LocationAddress]'))
ALTER TABLE [dbo].[LocationAddress] DROP CONSTRAINT [FK_LocationAddress_Location]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LocationAddress]') AND type in (N'U'))
DROP TABLE [dbo].[LocationAddress]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LocationAddress]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[LocationAddress](
	[locAddrID] [int] IDENTITY(1,1) NOT NULL,
	[locID] [int] NOT NULL,
	[locAddrName] [varchar](max) NULL,
 CONSTRAINT [PK_LocationAddress] PRIMARY KEY CLUSTERED 
(
	[locAddrID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_LocationAddress_Location]') AND parent_object_id = OBJECT_ID(N'[dbo].[LocationAddress]'))
ALTER TABLE [dbo].[LocationAddress]  WITH CHECK ADD  CONSTRAINT [FK_LocationAddress_Location] FOREIGN KEY([locID])
REFERENCES [dbo].[Location] ([locID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_LocationAddress_Location]') AND parent_object_id = OBJECT_ID(N'[dbo].[LocationAddress]'))
ALTER TABLE [dbo].[LocationAddress] CHECK CONSTRAINT [FK_LocationAddress_Location]
GO
SET IDENTITY_INSERT [dbo].[LocationAddress] ON
INSERT [dbo].[LocationAddress] ([locAddrID], [locID], [locAddrName]) VALUES (4, 14, N'ddd234')
SET IDENTITY_INSERT [dbo].[LocationAddress] OFF
