
/****** Object:  StoredProcedure [dbo].[DeleteCustomerEvent]    Script Date: 12/17/2015 16:08:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Soumalya
-- Create date: 16-12-2015
-- Description:	Delete customer

--  [DeleteCustomerEvent] --8
-- =============================================
CREATE PROCEDURE [dbo].[DeleteCustomerEvent]
	-- Add the parameters for the stored procedure here
	@customerId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DELETE from CustomerService where custID = @customerId
	DELETE from CustomerLocation where custID = @customerId
	DELETE from Customer where custID = @customerId
	
	
END
GO
