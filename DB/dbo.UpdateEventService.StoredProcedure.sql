USE [EventManagement]
GO
/****** Object:  StoredProcedure [dbo].[UpdateEventService]    Script Date: 12/15/2015 21:50:51 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UpdateEventService]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[UpdateEventService]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UpdateEventService]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		<Author,,Abhijit Mazumdar>
-- Create date: <Create Date,, 15/12/2015>
-- Description:	<Description,,Adds or updates new service to an event>
-- =============================================
CREATE PROCEDURE [dbo].[UpdateEventService] 
	-- Add the parameters for the stored procedure here
	@eventID int,
	@serviceID int,
	@serviceName varchar(max)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    if(@serviceID = 0)
    begin
	insert into service(svcName) values(@serviceName)
	
	declare @svcID int
	set @svcID = (select top 1 svcID from [service] order by svcID desc)
	
	insert into eventservice(evntID, svcID) values (@eventID, @svcID)
	end
	else
	begin
	update service set svcName = @serviceName where svcID = @serviceID
	end	
END
' 
END
GO
