USE [EventManagement]
GO
/****** Object:  Table [dbo].[Event]    Script Date: 12/15/2015 21:50:49 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Event]') AND type in (N'U'))
DROP TABLE [dbo].[Event]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Event]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Event](
	[evntID] [int] IDENTITY(1,1) NOT NULL,
	[evntName] [varchar](max) NOT NULL,
 CONSTRAINT [PK_Event] PRIMARY KEY CLUSTERED 
(
	[evntID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Event] ON
INSERT [dbo].[Event] ([evntID], [evntName]) VALUES (24, N'Test 1')
INSERT [dbo].[Event] ([evntID], [evntName]) VALUES (25, N'Test 2')
INSERT [dbo].[Event] ([evntID], [evntName]) VALUES (26, N'Test 3')
SET IDENTITY_INSERT [dbo].[Event] OFF
