﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace EventManagement.Models
{
    public class AccessUser
    {
        [Required(ErrorMessage = "Please Enter User Name")]
        [DisplayName("User Name")]
        public string UserName { get; set; }

        [Required(ErrorMessage = "Please Enter Password")]
        [DisplayName("Password")]
        public string Password { get; set; }

        [Required(ErrorMessage = "Please Enter Type")]
        [DisplayName("Type")]
        public string Type { get; set; }
        
        public string IsNewMember { get; set; }        
        public string IsActive { get; set; }
    }
}