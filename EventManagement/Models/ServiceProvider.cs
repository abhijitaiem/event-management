﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace EventManagement.Models
{
    public class ServiceProvider
    {
        public Int32 ProviderId { get; set; }

        [Required(ErrorMessage = "Please Enter Name")]
        [DisplayName("Name")]
        public string ProviderName { get; set; }

        [Required(ErrorMessage = "Please Enter Name")]
        [DisplayName("Company Name")]
        public string ProviderCompany { get; set; }

        [Required(ErrorMessage = "Please Enter Address")]
        [DisplayName("Address")]
        public string ProviderAddress { get; set; }

        [Required(ErrorMessage = "Please Enter Phone")]
        [StringLength(10, ErrorMessage = "The Mobile must contains 10 characters", MinimumLength = 10)]
        [DisplayName("Phone")]
        public string ProviderPhone { get; set; }

        [Required(ErrorMessage = "Please Enter Email")]
        [RegularExpression(@"^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$",
        ErrorMessage = "Please Enter Correct Email Address")]
        [DisplayName("Email")]
        public string ProviderEmail { get; set; }
        
        [DisplayName("Upload Images")]
        public int UploadImages { get; set; }

        public string Comment { get; set; }
        public Int32 UserID { get; set; }
    }
}