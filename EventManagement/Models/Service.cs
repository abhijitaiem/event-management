﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace EventManagement.Models
{
    public class Service
    {
        [Key]
        public Int32 ServiceId { get; set; }
        public string ServiceName { get; set; }
    }
}