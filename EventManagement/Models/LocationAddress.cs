﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace EventManagement.Models
{
    public class LocationAddress
    {
        [Key]
        public Int32 LocationAddressId { get; set; }
        [Required(ErrorMessage = "Please Enter Location Address")]
        [DisplayName("Location Address")]
        public string LocationAddressName { get; set; }
        [Required(ErrorMessage = "Please Select Location")]
        [DisplayName("Location")]
        public Int32 LocationId { get; set; }
    }
}