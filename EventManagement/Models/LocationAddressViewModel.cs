﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;


namespace EventManagement.Models
{
    public class LocationAddressViewModel
    {        
        public Int32 LocationAddressId { get; set; }        
        public string LocationAddressName { get; set; }        
        public Location Location { get; set; }        
    }
}