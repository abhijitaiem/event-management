﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System.Web.WebPages.Html;

namespace EventManagement.Models
{
    public class Customer
    {        
        public Int32 CustomerId { get; set; }

        [Required(ErrorMessage = "Please Enter Name")]
        [DisplayName("Name")]
        public string CustomerName { get; set; }

        [Required(ErrorMessage = "Please Enter Address")]
        [DisplayName("Address")]
        public string CustomerAddress { get; set; }

        [Required(ErrorMessage = "Please Enter Phone")]
        [StringLength(10, ErrorMessage = "The Mobile must contains 10 characters", MinimumLength = 10)]
        [DisplayName("Phone")]
        public string CustomerPhone { get; set; }

        [Required(ErrorMessage = "Please Enter Email")]
        [RegularExpression(@"^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$",
        ErrorMessage = "Please Enter Correct Email Address")]
        [DisplayName("Email")]
        public string CustomerEmail { get; set; }

        //[DataType(DataType.Date)]
        [DisplayName("Event Date")]
        //[DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd-mm-yyyy}")]
        public DateTime CustomerEventdate { get; set; }

        public Int32 EventId { get; set; }
        public bool isFixedLocation { get; set; }

        public Int32 LocationId { get; set; }
        public Int32 LocationAddId { get; set; }

        public string[] Selected { get; set; }
        public IEnumerable<SelectListItem> slServices { get; set; }

        public Int32 ReligianId { get; set; }
        [Required(ErrorMessage = "Please Enter Invitees")]
        [DisplayName("How many invitees you expect")]
        public string Invitees { get; set; }
        [DisplayName("Date From")]
        public DateTime DateFrom { get; set; }
        [DisplayName("Date To")]
        public DateTime DateTo { get; set; }
        [DataType(DataType.MultilineText)]
        [DisplayName("Special Ask")]
        public string SpecialAsk { get; set; }

        public Int32 userId { get; set; }
    }
}