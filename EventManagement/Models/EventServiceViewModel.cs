﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EventManagement.Models
{
    public class EventServiceViewModel
    {
        public Int32 EventId { get; set; }
        //public List<Service> Services { get; set; }
        public Int32 ServiceId { get; set; }
        public string ServiceName { get; set; }
    }
}