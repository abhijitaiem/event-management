﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace EventManagement.Models
{
    public class Location
    {
        [Key]
        public Int32 LocID { get; set; }
        [Required(ErrorMessage = "Please Enter Location Details")]
        [DisplayName("Location Details")]
        public string LocDetails { get; set; }
    }
}