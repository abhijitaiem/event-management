﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EventManagement.Models
{
    public class Enumerator
    {
       public enum UserType
        {
            Admin = 1,
            Standard = 2,
            ServiceProvider = 3,
            Event = 4,
            Other = 5
        };
    }
}