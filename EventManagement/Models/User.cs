﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System.Web.Mvc;

namespace EventManagement.Models
{
    public class UserViewModel
    {
        public Int32 UserID { get; set; }

        public string FullName { get; set; }

        [Required(ErrorMessage = "Username is required")]
        [DisplayName("UserName")]
        public string UserName { get; set; }

        [Required(ErrorMessage = "Password is required")]
        [StringLength(20)]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Required(ErrorMessage = "Confirm Password is required")]
        [StringLength(20)]
        [DataType(DataType.Password)]
        [Compare("Password", ErrorMessage = "Must match with Password")]
        public string ConfirmPassword { get; set; }

        public string UserType { get; set; }

        public bool IsNewMember { get; set; }

        public bool IsActive { get; set; }
    }
}