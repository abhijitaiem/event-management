﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EventManagement.Models
{
    public class Religian
    {
        public Int32 ReligianId { get; set; }
        public string ReligianName { get; set; }
    }
}