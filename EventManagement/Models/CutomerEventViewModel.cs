﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace EventManagement.Models
{
    public class CutomerEventViewModel
    {
        public Int32 CustomerId { get; set; }
        [DisplayName("Name")]
        public string CustomerName { get; set; }
        public string CustomerAddress { get; set; }
        [DisplayName("Phone")]
        public string CustomerPhone { get; set; }
        [DisplayName("Email")]
        public string CustomerEmail { get; set; }

        [DisplayName("Event Date")]
        //[DataType(DataType.Date)]        
        //[DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd-mm-yyyy}")]        
        public DateTime CustomerEventdate { get; set; }

        public Int32 EventId { get; set; }

        public bool isFixedLocation { get; set; }
        [DisplayName("Event Name")]
        public string EventName { get; set; }

        public Int32 LocationId { get; set; }
        public string LocationName { get; set; }

        public Int32 LocationAddId { get; set; }
        public string LocationAddname { get; set; }

        public List<string> lstServices = new List<string>();

        public string[] Selected { get; set; }
        public IEnumerable<SelectListItem> slServices { get; set; }

        public Int32 ReligianId { get; set; }        
        [DisplayName("How many invitees you expect")]
        public string Invitees { get; set; }

        [DisplayName("Date From")]
        //[DataType(DataType.Date)]
        //[DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd-mm-yyyy}")] 
        public DateTime DateFrom { get; set; }

        [DisplayName("Date To")]
        //[DataType(DataType.Date)]
        //[DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd-mm-yyyy}")] 
        public DateTime DateTo { get; set; }
        [DataType(DataType.MultilineText)]
        [DisplayName("Special Ask")]
        public string SpecialAsk { get; set; }

        public Int32 userId { get; set; }

        public string selectedServices { get; set; }

        public Int32 PeopleCount { get; set; }
    }
}