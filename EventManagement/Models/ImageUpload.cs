﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;

namespace EventManagement.Models
{
    public class ImageUpload
    {
        public Int32 Id { get; set; }
        public Int32 UploaderId { get; set; }
        [DisplayName("Upload Image")]
        public HttpPostedFileBase UploadImage { get; set; }
        [DisplayName("Image Name")]
        public string ImageName { get; set; }
        public string ImageMimeType { get; set; }
        public byte[] Image { get; set; }
    }
}