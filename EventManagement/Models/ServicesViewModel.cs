﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace EventManagement.Models
{
    public class ServicesViewModel
    {
        public Int32 ProviderId { get; set; }

        [Required(ErrorMessage = "Please Enter Name")]
        [DisplayName("Name")]
       // [StringLength(5, ErrorMessage = "The Mobile must contains 10 characters", MinimumLength = 1)]
        public string ProviderName { get; set; }

        [Required(ErrorMessage = "Please Enter Company Name")]
        [DisplayName("Company Name")]
        public string ProviderCompany { get; set; }

        [Required(ErrorMessage = "Please Enter Address")]
        [DisplayName("Address")]
        public string ProviderAddress { get; set; }

        [Required(ErrorMessage = "Please Enter Phone")]
        [StringLength(10, ErrorMessage = "The Mobile must contains 10 characters", MinimumLength = 10)]
        [DisplayName("Phone")]
        public string ProviderPhone { get; set; }

        [Required(ErrorMessage = "Please Enter Email")]
        [RegularExpression(@"^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$",
        ErrorMessage = "Please Enter Correct Email Address")]
        [DisplayName("Email")]
        public string ProviderEmail { get; set; }

        [DisplayName("Upload Images")]
        public int NoOfImages { get; set; }

        [DataType(DataType.MultilineText)]
        [DisplayName("Special Notes")]
        public string Comment { get; set; }
        public Int32 UserID { get; set; }

        public Int32 Id { get; set; } 
       
        [DisplayName("Upload Image")]
        public HttpPostedFileBase UploadImage { get; set; }
        [DisplayName("Image Name")]
        public string ImageName { get; set; }
        public string ImageMimeType { get; set; }


        [RequiredArray(ErrorMessage = "Please select Events")]
        public string[] Events { get; set; }
        public string[] Services { get; set; }

        [DisplayName("User Name")]
        public string UserName { get; set; }
        public string Password { get; set; }

        public string[] Selected { get; set; }
        public string selectedServices { get; set; }
        public string selectedEvents { get; set; }

        public Int32 ReligianId { get; set; }
        [Required(ErrorMessage = "Please Enter Invitees")]
        [DisplayName("Invitees")]
        public string Invitees { get; set; }
        [DisplayName("Special Ask")]
        public string SpecialAsp { get; set; }

    }
}