﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace EventManagement.Models
{
    public class Event
    {
        [Key]
        public Int32 EventId { get; set; }
        [Required(ErrorMessage = "Please Enter Event Name")]
        [DisplayName("Name")]
        public string EventName { get; set; }
    }
}