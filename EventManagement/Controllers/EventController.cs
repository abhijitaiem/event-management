﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SmsClient;
using System.Configuration;
using EventManagement.Models;


namespace EventManagement.Controllers
{
    public class EventController : Controller
    {
        //
        // GET: /Event/
        EventManagementEntities db = new EventManagementEntities();

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult CreateEvent()
        {

            try
            {
                //using (var db = new EventManagementEntities1())
                //{
                var events = from e in db.Events
                             select new
                             {
                                 id = e.evntID,
                                 name = e.evntName
                             };

                //var services = from e in db.Services
                //               select new
                //               {
                //                   ServiceId = e.svcID,
                //                   ServiceName = e.svcName
                //               };

                var locations = from e in db.Locations
                                select new
                                {
                                    LocationId = e.locID,
                                    LocationName = e.locDetails
                                };

                var religians = from r in db.Religions
                               select new
                               {
                                   ReligianId = r.relgID,
                                   ReligianName = r.relgName
                               };

                ViewBag.EventList = events;
                ViewBag.ReligianList = religians;
                ViewBag.LocationList = locations;

                //EventManagement.Models.Customer model = new Models.Customer();
                //// Create an empty list to hold result of the operation
                //var selectList = new List<SelectListItem>();

                //// For each string in the 'elements' variable, create a new SelectListItem object
                //// that has both its Value and Text properties set to a particular value.
                //// This will result in MVC rendering each item as:
                ////     <option value="State Name">State Name</option>
                //foreach (var element in services)
                //{
                //    selectList.Add(new SelectListItem
                //    {
                //        Value = element.ServiceId.ToString(),
                //        Text = element.ServiceName
                //    });
                //}

                //model.slServices = selectList;
                return View("CreateEvent");
                //}                


            }
            catch (System.Exception ex)
            {
                ModelState.AddModelError(ex.Source, ex.Message);
                return View("Error");
            }
        }
        [HttpPost]
        public ActionResult CreateEvent(EventManagement.Models.Customer model)
        {
            try
            {

                using (var db = new EventManagementEntities())
                {
                    //if (ModelState.IsValid)
                    //{

                        Customer cust = new Customer();

                        cust.custName = model.CustomerName;
                        cust.custEmail = model.CustomerEmail;
                        cust.custAddress = model.CustomerAddress;
                        cust.custPhone = model.CustomerPhone;
                        cust.evntDate = model.CustomerEventdate;
                        cust.evntID = model.EventId;
                        cust.custIsFixedLocation = model.isFixedLocation;
                        cust.relgID = model.ReligianId;
                        cust.custInvitees = model.Invitees;
                        cust.custDtFrom = model.DateFrom;
                        cust.custDtTo = model.DateTo;
                        cust.custSpacialAsk = model.SpecialAsk;
                    //cust.custUserId = 
                        db.Customers.AddObject(cust);
                        db.SaveChanges();

                        string strService = string.Empty;

                        //// Service Insert bulk
                        if (model.Selected.Length > 0)
                        {                            
                            foreach (var item in model.Selected)
                            {
                                strService += ","+ item.ToString();                                
                                
                            }

                            strService = strService.Remove(0, 1);

                            db.CreateEventServices(Convert.ToInt32(cust.custID), strService);
                        }

                        if (!model.isFixedLocation)
                        {

                            CustomerLocation custLocAdd = new CustomerLocation();
                            custLocAdd.locAddrID = model.LocationAddId;
                            custLocAdd.custID = cust.custID;

                            db.CustomerLocations.AddObject(custLocAdd);
                            db.SaveChanges();
                        }


                        //SendSms sms = new SendSms();
                        //string status = sms.send(ConfigurationManager.AppSettings["FromSms"].ToString(), ConfigurationManager.AppSettings["FromSmsPW"].ToString(), "Congratulation!You have successfully add event on" + model.CustomerEventdate.ToString("dd/MM/yyyy"), model.CustomerPhone);

                    // User Creation
                       string strPW = CreateRandomPassword(8);
                       User user = new User();
                       user.usUserName = model.CustomerPhone;
                       user.usPassword = strPW;
                       user.usIsActive = true;
                       user.usIsNewMember = true;
                       user.usType = EventManagement.Models.Enumerator.UserType.Event.ToString();

                       db.Users.AddObject(user);
                       db.SaveChanges();

                       if (user.usId > 0)
                       {
                           var original = db.Customers.Where(t => t.custID == cust.custID).FirstOrDefault();
                           original.custUserId = user.usId;
                           db.Customers.ApplyCurrentValues(original);
                           db.SaveChanges();
                       }
                       

                        return RedirectToAction("CreateEvent", "Home");
                    //}
                    //else
                    //{
                    //    return RedirectToAction("CreateEvent", "Event");
                    //}
                }
            }
            catch (System.Exception ex)
            {
                ModelState.AddModelError(ex.Source, ex.Message);
                //Response.Write(ex.Message);
                //return RedirectToAction("CreateEvent", "Event");
                
                return View("Error");
            }

        }

        public ActionResult EditCustomerEvent(Int32 customerId)
        {
            try
            {

                //using (var db = new EventManagementEntities())
                //{
                CutomerEventViewModel vmCustEvent = new CutomerEventViewModel();

                var events = from e in db.Events
                             select new
                             {
                                 id = e.evntID,
                                 name = e.evntName
                             };


                var locations = from e in db.Locations
                                select new
                                {
                                    LocationId = e.locID,
                                    LocationName = e.locDetails
                                };

                var religians = from r in db.Religions
                                select new
                                {
                                    ReligianId = r.relgID,
                                    ReligianName = r.relgName
                                };

                ViewBag.EventList = events; //new SelectList(events, "id", "name", selected); ;
                ViewBag.ReligianList = religians;
                ViewBag.LocationList = locations;




                var customerDetails = (from c in db.Customers
                                       join ev in db.Events on c.evntID equals ev.evntID
                                       join cl in db.CustomerLocations on c.custID equals cl.custID into py
                                       from cl in py.DefaultIfEmpty()
                                       join la in db.LocationAddresses on cl.locAddrID equals la.locAddrID into ps
                                       from la in ps.DefaultIfEmpty()
                                       join l in db.Locations on la.locID equals l.locID into pz
                                       from l in pz.DefaultIfEmpty()
                                       where c.custID == customerId
                                       select new
                                       {
                                           CustomerId = customerId,
                                           CustomerName = c.custName,
                                           CustomerAddress = c.custAddress,
                                           CustomerPhone = c.custPhone,
                                           CustomerEmail = c.custEmail,
                                           CustomerEventdate = c.evntDate.HasValue ? c.evntDate.Value : DateTime.MinValue,
                                           ReligianId = c.relgID,
                                           Invitees = c.custInvitees,
                                           DateFrom = c.custDtFrom.HasValue ? c.custDtFrom.Value : DateTime.MinValue,
                                           DateTo = c.custDtTo.HasValue ? c.custDtTo.Value : DateTime.MinValue,
                                           SpecialAsk = c.custSpacialAsk,
                                           EventId = c.evntID.HasValue ? c.evntID.Value : Int32.MinValue,
                                           isFixedLocation = c.custIsFixedLocation.HasValue ? c.custIsFixedLocation.Value : false,
                                           LocationId = (l.locID == 0 || l.locID == null) ? Int32.MinValue : l.locID,
                                           LocationName = l.locDetails == null ? string.Empty : l.locDetails,
                                           LocationAddId = (la.locAddrID == 0 || la.locAddrID == null) ? Int32.MinValue : la.locAddrID,
                                           LocationAddname = la.locAddrName == null ? string.Empty : la.locAddrName
                                       });

                if (customerDetails.Count() > 0 && customerDetails != null)
                {
                    vmCustEvent.CustomerId = customerDetails.FirstOrDefault().CustomerId;
                    vmCustEvent.CustomerName = customerDetails.FirstOrDefault().CustomerName;
                    vmCustEvent.CustomerAddress = customerDetails.FirstOrDefault().CustomerAddress;
                    vmCustEvent.CustomerPhone = customerDetails.FirstOrDefault().CustomerPhone;
                    vmCustEvent.CustomerEmail = customerDetails.FirstOrDefault().CustomerEmail;
                    vmCustEvent.CustomerEventdate = Convert.ToDateTime(customerDetails.FirstOrDefault().CustomerEventdate);
                    vmCustEvent.EventId = Convert.ToInt32(customerDetails.FirstOrDefault().EventId);
                    vmCustEvent.isFixedLocation = Convert.ToBoolean(customerDetails.FirstOrDefault().isFixedLocation);
                    vmCustEvent.LocationId = customerDetails.FirstOrDefault().LocationId;
                    vmCustEvent.LocationName = customerDetails.FirstOrDefault().LocationName;
                    vmCustEvent.LocationAddId = customerDetails.FirstOrDefault().LocationAddId;
                    vmCustEvent.LocationAddname = customerDetails.FirstOrDefault().LocationAddname;
                    vmCustEvent.ReligianId = Convert.ToInt32(customerDetails.FirstOrDefault().ReligianId);
                    vmCustEvent.Invitees = customerDetails.FirstOrDefault().Invitees;
                    vmCustEvent.SpecialAsk = customerDetails.FirstOrDefault().SpecialAsk;
                    vmCustEvent.DateFrom = customerDetails.FirstOrDefault().DateFrom;
                    vmCustEvent.DateTo = customerDetails.FirstOrDefault().DateTo;

                    var custService = from cs in db.CustomerServices
                                      where cs.custID == customerId
                                      select new { SericeId = cs.svcID };
                    string strService = string.Empty;

                    foreach (var i in custService)
                    {
                        vmCustEvent.lstServices.Add(Convert.ToString(i.SericeId));
                        strService += "," + i.SericeId.ToString();
                    }

                    vmCustEvent.Selected = vmCustEvent.lstServices.ToArray();
                    vmCustEvent.selectedServices = strService.Remove(0, 1);
                }


                return View("EditCustomerEvent", vmCustEvent);
                //}
            }
            catch (System.Exception ex)
            {
                ModelState.AddModelError(ex.Source, ex.Message);
                return View("Error");
            }
        }
        [HttpPost]
        public ActionResult EditCustomerEvent(CutomerEventViewModel model,Int32 cId)
        {
            try
            {
                using (var db = new EventManagementEntities())
                {

                    var original = db.Customers.Where(t => t.custID == model.CustomerId).FirstOrDefault();

                    Customer cust = new Customer();

                    original.custName = model.CustomerName;
                    original.custEmail = model.CustomerEmail;
                    original.custAddress = model.CustomerAddress;
                    original.custPhone = model.CustomerPhone;
                    original.evntDate = model.CustomerEventdate;
                    original.evntID = model.EventId;
                    original.custIsFixedLocation = model.isFixedLocation;
                    //vmCustEvent.LocationId = customerDetails.FirstOrDefault().LocationId;
                    //vmCustEvent.LocationName = customerDetails.FirstOrDefault().LocationName;
                    //vmCustEvent.LocationAddId = customerDetails.FirstOrDefault().LocationAddId;
                    //vmCustEvent.LocationAddname = customerDetails.FirstOrDefault().LocationAddname;
                    original.relgID = Convert.ToInt32(model.ReligianId);
                    original.custInvitees = model.Invitees;
                    original.custSpacialAsk = model.SpecialAsk;
                    original.custDtFrom = model.DateFrom;
                    original.custDtTo = model.DateTo;
                    db.Customers.ApplyCurrentValues(original);
                    db.SaveChanges();

                    string strService = string.Empty;

                    //// Service Insert bulk
                    if (model.Selected.Length > 0)
                    {
                        foreach (var item in model.Selected)
                        {
                            strService += "," + item.ToString();

                        }

                        strService = strService.Remove(0, 1);

                        db.UpdateCustomerEventServices(cId, strService);
                    }


                    if (!model.isFixedLocation)
                    {

                        CustomerLocation custLocAdd = new CustomerLocation();
                        var custLoactionAdd = db.CustomerLocations.Where(t => t.custID == model.CustomerId).FirstOrDefault();


                        custLoactionAdd.locAddrID = model.LocationAddId;
                        custLoactionAdd.custID = cId;
                        //db.CustomerLocations.Attach(custLoactionAdd);
                        //db.ObjectStateManager.ChangeEntityState(
                        db.CustomerLocations.ApplyCurrentValues(custLoactionAdd);
                        db.SaveChanges();
                    }


                    return RedirectToAction("EditEvent", "Home");
                }
            }
            catch (System.Exception ex)
            {
                ModelState.AddModelError(ex.Source, ex.Message);
                return View("Error");
            }
        }

        public ActionResult GetEvents()
        {
            try
            {


                List<CutomerEventViewModel> lstCustEvent = new List<CutomerEventViewModel>();
                //CutomerEventViewModel vmCustEvent = new CutomerEventViewModel();

                var details = from c in db.Customers
                              join e in db.Events on c.evntID equals e.evntID
                              select new
                              {
                                  CustomerId = c.custID,
                                  CustomerEmail = c.custEmail,
                                  CustomerName = c.custName,
                                  EventDt = c.evntDate.HasValue ? c.evntDate.Value : DateTime.MinValue,
                                  isFixedLocation = c.custIsFixedLocation.HasValue ? c.custIsFixedLocation.Value : false,
                                  EventName = e.evntName,
                                  Phone = c.custPhone
                              };

                foreach (var item in details)
                {
                    CutomerEventViewModel vmCustEvent = new CutomerEventViewModel();

                    vmCustEvent.CustomerId = item.CustomerId;
                    vmCustEvent.CustomerEmail = item.CustomerEmail;
                    vmCustEvent.CustomerPhone = item.Phone;
                    vmCustEvent.CustomerName = item.CustomerName;
                    vmCustEvent.CustomerEventdate = item.EventDt;
                    vmCustEvent.isFixedLocation = item.isFixedLocation;
                    vmCustEvent.EventName = item.EventName;

                    lstCustEvent.Add(vmCustEvent);
                }

                return View(lstCustEvent);
            }
            catch (System.Exception ex)
            {
                ModelState.AddModelError(ex.Source, ex.Message);
                return RedirectToAction("CreateEvent", "Event");
            }
        }

        public ActionResult DeleteCustomerEvent(Int32 customerId)
        {
            try
            {
                
                db.DeleteCustomerEvent(customerId);
                return RedirectToAction("GetEvents");
            }
            catch (System.Exception ex)
            {
                ModelState.AddModelError(ex.Source, ex.Message);
                return View("Error");
            }

            
        }

        public static string CreateRandomPassword(int PasswordLength)
        {
            string _allowedChars = "0123456789abcdefghijkmnopqrstuvwxyzABCDEFGHJKLMNOPQRSTUVWXYZ";
            Random randNum = new Random();
            char[] chars = new char[PasswordLength];
            int allowedCharCount = _allowedChars.Length;
            for (int i = 0; i < PasswordLength; i++)
            {
                chars[i] = _allowedChars[(int)((_allowedChars.Length) * randNum.NextDouble())];
            }
            return new string(chars);
        }


    }
}
