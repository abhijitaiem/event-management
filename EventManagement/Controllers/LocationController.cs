﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EventManagement.Controllers
{
    public class LocationController : Controller
    {
        //
        // GET: /Location/

        public ActionResult GetAllLocation()
        {
            return View();
        }

        public ActionResult GetPlaceByLocationId(Int32 locationId)
        {
            EventManagementEntities db = new EventManagementEntities();

            var locationAdd = from e in db.LocationAddresses
                              where e.locID == locationId
                              select new
                              {
                                  id = e.locAddrID,
                                  name = e.locAddrName
                              };

            //db.LocationAddresses.Where(c => c.locID == locationId);
            return Json(locationAdd, JsonRequestBehavior.AllowGet);

        }
    }
}
