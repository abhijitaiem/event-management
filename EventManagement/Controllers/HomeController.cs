﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UserModel = EventManagement.Models.UserViewModel;

namespace EventManagement.Controllers
{
    public class HomeController : Controller
    {

        EventManagementEntities db = new EventManagementEntities();

        //
        // GET: /Home/

        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Contact()
        {
            return View();
        }
        public ActionResult CreateEvent()
        {
            return RedirectToAction("CreateEvent", "Event");
        }
        public ActionResult EditEvent()
        {
            return RedirectToAction("GetEvents", "Event");
        }

        [HttpGet]
        public ActionResult Login()//UserModel model)
        {
            //try
            //{
            //    UserModel user = GetUserDetails(model); 

                return View();
            //}
            //catch (Exception ex)
            //{
            //    ModelState.AddModelError(ex.Source, ex.Message);
            //    return RedirectToAction("Login", "Home");
            //}
        }

        [HttpPost]
        public ActionResult Login(UserModel model)
        {
            try
            {
                ModelState.Remove("ConfirmPassword");
                if (ModelState.IsValid)
                {
                    UserModel user = GetUserDetails(model);

                    if (user != null)
                    {
                        if (user.IsNewMember)
                        {
                            User usr = db.Users.FirstOrDefault(e => e.usId == user.UserID);
                            if(usr != null)
                                usr.usIsNewMember = false;
                            db.SaveChanges();
                        }

                        if (string.Compare(user.UserType, "Customer", false) == 0)
                            return RedirectToAction("GetAllServiceProvider", "Occation");
                        else if (string.Compare(user.UserType, "Provider", false) == 0)
                            return RedirectToAction("GetAllEvent", "Occation");
                    }                    
                }
                
                return View(new UserModel());
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(ex.Source, ex.Message);
                return RedirectToAction("Login", "Home");
            }
        }

        private UserModel GetUserDetails(UserModel model)
        {            
            UserModel user = (from u in db.Users
                              where u.usUserName == model.UserName
                              && u.usPassword == model.Password
                              && u.usIsActive == true                              
                              select new UserModel
                              {
                                  UserID = u.usId,
                                  UserType = u.usType,
                                  IsNewMember = u.usIsNewMember.Value
                              }).FirstOrDefault();

            return user;
        }
    }
}