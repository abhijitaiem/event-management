﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Newtonsoft.Json;
using EventManagement.Models;
using System.Data.Objects;
using EventManagement.Utilities;

namespace EventManagement.Controllers
{
    public class OccationController : Controller
    {
        //
        // GET: /Occation/

        EventManagementEntities db = new EventManagementEntities();

        public ActionResult GetAllEvent()
        {
            try
            {
                string dateRange = Request.Form["ddlDateFilter"];
                List<CutomerEventViewModel> events = new List<CutomerEventViewModel>();
                EventManagementEntities db = new EventManagementEntities();
                if (dateRange != null)
                {
                    double startRange = 0, endRange = 0;
                    if (dateRange == "less than 50 days")
                    {
                        startRange = 1;
                        endRange = 49;
                    }
                    else if (dateRange == "50-100 days")
                    {
                        startRange = 50;
                        endRange = 100;
                    }
                    else if (dateRange == "greater than 100 days")
                    {
                        startRange = 101;
                        endRange = 365;
                    }
                    events = (from c in db.Customers
                              join e in db.Events on c.evntID equals e.evntID
                              where c.custDtFrom.HasValue
                              && c.custDtTo.HasValue
                              && EntityFunctions.DiffDays(c.custDtTo.Value, DateTime.Now) > startRange
                              && EntityFunctions.DiffDays(c.custDtTo.Value, DateTime.Now) < endRange
                              select new CutomerEventViewModel
                              {
                                  EventId = e.evntID,
                                  EventName = e.evntName,
                                  CustomerName = c.custName,
                                  CustomerEmail = c.custEmail,
                                  PeopleCount = 0,
                                  DateFrom = c.custDtFrom.Value,
                                  DateTo = c.custDtTo.Value,
                                  SpecialAsk = c.custSpacialAsk
                              }).ToList();
                }
                else
                {
                    events = (from c in db.Customers
                              join e in db.Events on c.evntID equals e.evntID
                              where c.custDtFrom.HasValue && c.custDtTo.HasValue
                              select new CutomerEventViewModel
                              {
                                  EventId = e.evntID,
                                  EventName = e.evntName,
                                  CustomerName = c.custName,
                                  CustomerEmail = c.custEmail,
                                  PeopleCount = 0,
                                  DateFrom = c.custDtFrom.Value,
                                  DateTo = c.custDtTo.Value,
                                  SpecialAsk = c.custSpacialAsk
                              }).ToList();
                }

                return View(events);
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(ex.Source, ex.Message);
                return RedirectToAction("CreateEvent", "Event");
            }
        }

        public void SendEmail(FormCollection form)
        {
            try
            {
                var selectedIds = form.GetValues("chkSelected");
                if (selectedIds != null)
                {
                    EventManagementEntities db = new EventManagementEntities();
                    foreach (var id in selectedIds)
                    {
                        if (id != "false")
                        {
                            string email = (from c in db.Customers where c.evntID.Value.ToString() == id select c.custEmail).FirstOrDefault();
                            if (!string.IsNullOrEmpty(email))
                            {
                                //SendEmail                               
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(ex.Source, ex.Message);
            }
        }

        public ActionResult GetPlaceByEventId(Int32 eventId)
        {
            EventManagementEntities db = new EventManagementEntities();

            var services = from s in db.Services
                           join es in db.EventServices on s.svcID equals es.svcID
                           join e in db.Events on es.evntID equals e.evntID
                           where e.evntID == eventId
                           select new
                           {
                               id = s.svcID,
                               name = s.svcName
                           };

            //db.LocationAddresses.Where(c => c.locID == locationId);
            return Json(services, JsonRequestBehavior.AllowGet);

        }

        public ActionResult GetPlaceByEventIds(string eventIds)
        {
            EventManagementEntities db = new EventManagementEntities();

            //var services = from s in db.Services
            //               join es in db.EventServices on s.svcID equals es.svcID
            //               join e in db.Events on es.evntID equals e.evntID
            //               where eventIds.Contains(e.evntID.ToString()) //= (eventIds
            //               select new
            //               {
            //                   id = s.svcID,
            //                   name = s.svcName
            //               };

            //JavaScriptSerializer serializer = new JavaScriptSerializer();
            //var obj = serializer.Deserialize<Dictionary<string, object>>(eventIds);

            dynamic dynObj = JsonConvert.DeserializeObject(eventIds);
            //Array dd = new Array[] {  dynObj.eId  };

            var arr = dynObj["eId"];

             //int[] myInts = Array.ConvertAll(arr, int.Parse);

            string ed = string.Empty;

            foreach (var it in arr)
            {
                ed += "," + it.ToString();
            }
            ed = ed.Remove(0, 1);

            var idList = new string[] { };
            idList = ed.Split(',');
            int[] myInts = Array.ConvertAll(idList, int.Parse);


            var services = from s in db.Services
                           join es in db.EventServices on s.svcID equals es.svcID
                           join e in db.Events on es.evntID equals e.evntID
                           where myInts.Contains(e.evntID)
                           select new
                           {
                               id = s.svcID,
                               name = s.svcName
                           };

            //db.LocationAddresses.Where(c => c.locID == locationId);
            return Json(services, JsonRequestBehavior.AllowGet);

        }

        public ActionResult GetAllServiceProvider()
        {
            try
            {
                List<ServicesViewModel> lstsSrvicePro = new List<ServicesViewModel>();
                //CutomerEventViewModel vmCustEvent = new CutomerEventViewModel();

                var details = from sp in db.ServiceProviders
                              select new
                              {
                                  Id = sp.spId,
                                  ProviderEmail = sp.spEmail,
                                  Providername = sp.spName,
                                  ProviderPhone = sp.spPhone,
                                  NoOfImages = sp.spUploadImage,
                                  Company = sp.spCompanyName
                              };

                foreach (var item in details)
                {
                    ServicesViewModel vmProvider = new ServicesViewModel();

                    vmProvider.Id = item.Id;
                    vmProvider.ProviderName = item.Providername;
                    vmProvider.ProviderEmail = item.ProviderEmail;
                    vmProvider.ProviderPhone = item.ProviderPhone;
                    vmProvider.NoOfImages = Convert.ToInt16(item.NoOfImages);
                    vmProvider.ProviderCompany = item.Company;

                    lstsSrvicePro.Add(vmProvider);
                }

                return View("GetAllServiceProvider", lstsSrvicePro);
            }
            catch (System.Exception ex)
            {
                ModelState.AddModelError(ex.Source, ex.Message);
                return RedirectToAction("CreateEvent", "Event");
            }
        }

        public ActionResult DisplaySPImages(int id)
        {
            var lstimages = (from sp in db.ServiceProviders
                             join img in db.ImageUploads on sp.spId equals img.ImgUploaderId
                             where sp.spId == id
                             select new EventManagement.Models.ImageUpload
                             {
                                 Image = img.Image
                             });
            return PartialView("_GetSPImages", lstimages);
        }

    }
}
