﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Model = EventManagement.Models;
using System.Data.Objects;

namespace EventManagement.Controllers
{
    public class AdminController : Controller
    {
        //
        // GET: /Admin/

        EventManagementEntities db = new EventManagementEntities();

        public ActionResult ShowEvents()
        {
            try
            {
                List<Model.Event> lstModelEvent = new List<Model.Event>();
                List<Event> events = (from e in db.Events select e).ToList<Event>();
                foreach (Event e in events)
                {
                    Model.Event evnt = new Model.Event();
                    evnt.EventId = e.evntID;
                    evnt.EventName = e.evntName;
                    lstModelEvent.Add(evnt);
                }
                return View("ShowEvents", lstModelEvent);
            }
            catch (System.Exception ex)
            {
                ModelState.AddModelError(ex.Source, ex.Message);
                return View("Event");
            }
        }

        [HttpGet]
        public ActionResult CreateEvent(int? id)
        {
            Model.Event modelEvent = null;
            try
            {
                if (id != null)
                {
                    Event evnt = (from e in db.Events where e.evntID == id select e).FirstOrDefault();
                    if (evnt != null)
                    {
                        modelEvent = new Model.Event { EventId = evnt.evntID, EventName = evnt.evntName };
                    }
                }
                return View("Event", modelEvent);
            }
            catch (System.Exception ex)
            {
                ModelState.AddModelError(ex.Source, ex.Message);
                return View("Event", modelEvent);
            }
        }

        [HttpPost]
        public ActionResult SaveEvent(Model.Event model)
        {
            try
            {
                ModelState.Remove("EventId");
                if (ModelState.IsValid)
                {
                    Event evnt = null;
                    if (model.EventId == 0)
                    {
                        evnt = new Event();
                        evnt.evntName = model.EventName;
                        db.Events.AddObject(evnt);
                    }
                    else
                    {
                        evnt = db.Events.FirstOrDefault(e => e.evntID == model.EventId);
                        if (evnt != null)
                        {
                            evnt.evntName = model.EventName;
                        }
                    }
                    db.SaveChanges();
                    return RedirectToAction("ShowEvents");
                }
                else
                {
                    ModelState.AddModelError("Error", "Event name is required");
                    return RedirectToAction("CreateEvent");
                }
            }
            catch (System.Exception ex)
            {
                ModelState.AddModelError(ex.Source, ex.Message);
                return View("Event");
            }
        }

        public ActionResult DeleteEvent(int? id)
        {
            try
            {
                if (id != null)
                {
                    Event evnt = db.Events.FirstOrDefault(e => e.evntID == id);
                    if (evnt != null)
                    {
                        db.Events.DeleteObject(evnt);
                    }
                    db.SaveChanges();
                }
            }
            catch (System.Exception ex)
            {
                ModelState.AddModelError(ex.Source, ex.Message);
            }
            return RedirectToAction("ShowEvents");
        }

        public ActionResult EventService()
        {
            try
            {
                var events = from e in db.Events
                             select new
                             {
                                 id = e.evntID,
                                 name = e.evntName
                             };
                ViewBag.EventList = events;

                return View();
            }
            catch (System.Exception ex)
            {
                ModelState.AddModelError(ex.Source, ex.Message);
                return View("Event");
            }
        }

        public ActionResult ShowLocations(Location model)
        {
            try
            {
                List<Model.Location> lstModelLocation = new List<Model.Location>();
                List<Location> locations = (from l in db.Locations select l).ToList<Location>();
                foreach (Location e in locations)
                {
                    Model.Location loc = new Model.Location();
                    loc.LocID = e.locID;
                    loc.LocDetails = e.locDetails;
                    lstModelLocation.Add(loc);
                }
                return View("ShowLocations", lstModelLocation);
            }
            catch (System.Exception ex)
            {
                ModelState.AddModelError(ex.Source, ex.Message);
                return View("ShowLocations");
            }
        }

        [HttpGet]
        public ActionResult CreateLocation(int? id)
        {
            Model.Location modelLocation = null;
            try
            {
                if (id != null)
                {
                    Location loc = (from e in db.Locations where e.locID == id select e).FirstOrDefault();
                    if (loc != null)
                    {
                        modelLocation = new Model.Location { LocID = loc.locID, LocDetails = loc.locDetails };
                    }
                }
                return View("Location", modelLocation);
            }
            catch (System.Exception ex)
            {
                ModelState.AddModelError(ex.Source, ex.Message);
                return View("Location", modelLocation);
            }
        }

        [HttpPost]
        public ActionResult SaveLocation(Model.Location model)
        {
            try
            {
                ModelState.Remove("LocID");
                if (ModelState.IsValid)
                {
                    Location loc = null;
                    if (model.LocID == 0)
                    {
                        loc = new Location();
                        loc.locDetails = model.LocDetails;
                        db.Locations.AddObject(loc);
                    }
                    else
                    {
                        loc = db.Locations.FirstOrDefault(l => l.locID == model.LocID);
                        if (loc != null)
                        {
                            loc.locDetails = model.LocDetails;
                        }
                    }
                    db.SaveChanges();
                    return RedirectToAction("ShowLocations");
                }
                else
                {
                    ModelState.AddModelError("Error", "Location detail is required");
                    return RedirectToAction("CreateLocation");
                }
            }
            catch (System.Exception ex)
            {
                ModelState.AddModelError(ex.Source, ex.Message);
                return View("Location");
            }
        }

        public ActionResult DeleteLocation(int? id)
        {
            try
            {
                if (id != null)
                {
                    Location loc = db.Locations.FirstOrDefault(e => e.locID == id);
                    if (loc != null)
                    {
                        db.Locations.DeleteObject(loc);
                    }
                    db.SaveChanges();
                }
            }
            catch (System.Exception ex)
            {
                ModelState.AddModelError(ex.Source, ex.Message);
            }
            return RedirectToAction("ShowLocations");
        }

        public ActionResult ShowLocationAddress()
        {
            try
            {
                List<Model.LocationAddressViewModel> lstModelLocationAddress = new List<Model.LocationAddressViewModel>();
                List<LocationAddress> locationAddresses = (from l in db.LocationAddresses select l).ToList<LocationAddress>();
                foreach (LocationAddress e in locationAddresses)
                {
                    Model.LocationAddressViewModel loc = new Model.LocationAddressViewModel();
                    loc.LocationAddressId = e.locAddrID;
                    loc.LocationAddressName = e.locAddrName;
                    Location l = (from x in db.Locations where x.locID == e.locID select x).FirstOrDefault();
                    Model.Location mL = new Model.Location { LocID = l.locID, LocDetails = l.locDetails };
                    loc.Location = mL;
                    lstModelLocationAddress.Add(loc);
                }
                return View("ShowLocationAddress", lstModelLocationAddress);
            }
            catch (System.Exception ex)
            {
                ModelState.AddModelError(ex.Source, ex.Message);
                return View("ShowLocationAddress");
            }
        }

        [HttpGet]
        public ActionResult CreateLocationAddress(int? id)
        {
            Model.LocationAddress modelLocationAddress = null;
            try
            {
                var locations = from e in db.Locations
                                select new
                                {
                                    id = e.locID,
                                    name = e.locDetails
                                };
                ViewBag.LocationList = locations;
                if (id != null)
                {
                    LocationAddress loc = (from e in db.LocationAddresses where e.locAddrID == id select e).FirstOrDefault();
                    if (loc != null)
                    {
                        modelLocationAddress = new Model.LocationAddress { LocationAddressId = loc.locAddrID, LocationAddressName = loc.locAddrName, LocationId = loc.locID };
                    }
                }
                return View("LocationAddress", modelLocationAddress);
            }
            catch (System.Exception ex)
            {
                ModelState.AddModelError(ex.Source, ex.Message);
                return View("LocationAddress", modelLocationAddress);
            }
        }

        [HttpPost]
        public ActionResult SaveLocationAddress(Model.LocationAddress model)
        {
            try
            {
                ModelState.Remove("LocationAddressId");
                ModelState.Remove("LocationId");
                if (ModelState.IsValid)
                {
                    LocationAddress loc = null;
                    if (model.LocationAddressId == 0)
                    {
                        loc = new LocationAddress();
                        loc.locAddrName = model.LocationAddressName;
                        loc.locID = model.LocationId;
                        db.LocationAddresses.AddObject(loc);
                    }
                    else
                    {
                        loc = db.LocationAddresses.FirstOrDefault(l => l.locAddrID == model.LocationAddressId);
                        if (loc != null)
                        {
                            loc.locAddrName = model.LocationAddressName;
                            loc.locID = model.LocationId;
                        }
                    }
                    db.SaveChanges();
                    return RedirectToAction("ShowLocationAddress");
                }
                else
                {
                    ModelState.AddModelError("Error", "Location Address is required");
                    return RedirectToAction("CreateLocationAddress");
                }
            }
            catch (System.Exception ex)
            {
                ModelState.AddModelError(ex.Source, ex.Message);
                return View("LocationAddress");
            }
        }

        public ActionResult DeleteLocationAddress(int? id)
        {
            try
            {
                if (id != null)
                {
                    LocationAddress loc = db.LocationAddresses.FirstOrDefault(e => e.locAddrID == id);
                    if (loc != null)
                    {
                        db.LocationAddresses.DeleteObject(loc);
                    }
                    db.SaveChanges();
                }
            }
            catch (System.Exception ex)
            {
                ModelState.AddModelError(ex.Source, ex.Message);
            }
            return RedirectToAction("ShowLocationAddress");
        }

        public ActionResult ShowEventService()
        {
            List<Model.Event> events = (from e in db.Events
                                        select new Model.Event
                                          {
                                              EventId = e.evntID,
                                              EventName = e.evntName
                                          }).ToList();
            ViewBag.EventList = events;
            return PartialView("ShowEventServices");
        }

        public ActionResult ShowServices(int? EventId)
        {
            try
            {
                List<Model.EventServiceViewModel> lstSVC = new List<Model.EventServiceViewModel>();
                if (EventId != null)
                {
                    List<Service> svc = (from s in db.Services
                                         join es in db.EventServices on s.svcID equals es.svcID
                                         where es.evntID == EventId
                                         select s).ToList();
                    if (svc != null)
                    {
                        foreach (Service s in svc)
                        {
                            Model.EventServiceViewModel srv = new Model.EventServiceViewModel();
                            srv.EventId = EventId.Value;
                            srv.ServiceId = s.svcID;
                            srv.ServiceName = s.svcName;
                            lstSVC.Add(srv);
                        }
                    }
                }
                return View("ShowEventService", lstSVC);
            }
            catch (System.Exception ex)
            {
                ModelState.AddModelError(ex.Source, ex.Message);
                return View("ShowEventService");
            }
        }

        public ActionResult CreateEventService(int? eid, int? sid)
        {
            try
            {
                List<Model.Event> events = (from e in db.Events
                                            select new Model.Event
                                            {
                                                EventId = e.evntID,
                                                EventName = e.evntName
                                            }).ToList();
                ViewBag.EventList = events;
                Model.EventServiceViewModel srv = null;
                if (sid != 0)
                {
                    Service svc = (from s in db.Services
                                   where s.svcID == sid
                                   select s).FirstOrDefault();
                    if (svc != null)
                    {
                        srv = new Model.EventServiceViewModel();
                        srv.EventId = eid.Value;
                        srv.ServiceId = svc.svcID;
                        srv.ServiceName = svc.svcName;
                    }
                }
                return View("EventService",srv);
            }
            catch (System.Exception ex)
            {
                ModelState.AddModelError(ex.Source, ex.Message);
                return View("EventService");
            }
        }

        [HttpPost]
        public ActionResult SaveEventService(Model.EventServiceViewModel model)
        {
            try
            {
                ModelState.Remove("EventId");
                ModelState.Remove("ServiceId");
                if (ModelState.IsValid)
                {
                    db.UpdateEventService(model.EventId,model.ServiceId,model.ServiceName);
                    return RedirectToAction("ShowServices");
                }
                else
                {
                    ModelState.AddModelError("Error", "Service Name is required");
                    return RedirectToAction("CreateEventService");
                }
            }
            catch (System.Exception ex)
            {
                ModelState.AddModelError(ex.Source, ex.Message);
                return View("EventService");
            }
        }

        public ActionResult DeleteEventService(int? eid, int? sid)
        {
            try
            {
                if (eid != null && sid != null)
                {
                    db.DeleteEventService(eid, sid);
                }
            }
            catch (System.Exception ex)
            {
                ModelState.AddModelError(ex.Source, ex.Message);
            }
            return RedirectToAction("ShowServices");
        }        
    }
}