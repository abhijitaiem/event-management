﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using EventManagement.Models;
using System.Transactions;

namespace EventManagement.Controllers
{
    public class ServiceProviderController : Controller
    {
        EventManagementEntities db = new EventManagementEntities();

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetAllServiceProvider()
        {
            try
            {


                List<ServicesViewModel> lstsSrvicePro = new List<ServicesViewModel>();
                //CutomerEventViewModel vmCustEvent = new CutomerEventViewModel();

                var details = from sp in db.ServiceProviders                              
                              select new
                              {
                                  Id = sp.spId,
                                  ProviderEmail = sp.spEmail,
                                  Providername = sp.spName,
                                  ProviderPhone = sp.spPhone,
                                  NoOfImages = sp.spUploadImage,
                                  Company = sp.spCompanyName
                                  
                              };

                foreach (var item in details)
                {
                    ServicesViewModel vmProvider = new ServicesViewModel();

                    vmProvider.ProviderId = item.Id;
                    vmProvider.ProviderEmail = item.ProviderEmail;
                    vmProvider.ProviderPhone = item.ProviderPhone;
                    vmProvider.ProviderName = item.Providername;
                    vmProvider.NoOfImages = Convert.ToInt16(item.NoOfImages);
                    vmProvider.ProviderCompany = item.Company;

                    lstsSrvicePro.Add(vmProvider);
                }

                return View(lstsSrvicePro);
            }
            catch (System.Exception ex)
            {
                ModelState.AddModelError(ex.Source, ex.Message);
                return View("Error");
            }

            
        }

        public ActionResult CreateService()
        {
            try
            {
                var events = from e in db.Events
                             select new
                             {
                                 EventId = e.evntID,
                                 EventName = e.evntName
                             };

                var religians = from r in db.Religions
                                select new
                                {
                                    ReligianId = r.relgID,
                                    ReligianName = r.relgName
                                };

                ViewBag.Events = events;
                ViewBag.ReligianList = religians;

                return View();
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(ex.Source, ex.Message);
                return View("Error");
            }
        }

        [HttpPost]
        public ActionResult CreateService(ServicesViewModel model)
        {
            try
            {
                if (model.Selected != null)
                {
                    using (TransactionScope transaction = new TransactionScope())
                    {


                        using (var db1 = new EventManagementEntities())
                        {
                            ServiceProvider sp = new ServiceProvider();

                            sp.spName = model.ProviderName;
                            sp.spCompanyName = model.ProviderCompany;
                            sp.spAddress = model.ProviderAddress;
                            sp.spPhone = model.ProviderPhone;
                            sp.spEmail = model.ProviderEmail;
                            sp.spComment = model.Comment;
                            sp.spUploadImage = model.NoOfImages;
                            sp.spInvitees = model.Invitees;
                            sp.spRelId = model.ReligianId;

                            db1.ServiceProviders.AddObject(sp);
                            db1.SaveChanges();

                            string strService = string.Empty;
                            string strEvent = string.Empty;
                            

                            //// Service Insert bulk
                            if (model.Selected != null)
                            {
                                if (model.Selected.Length > 0)
                                {
                                    foreach (var item in model.Selected)
                                    {
                                        strService += "," + item.ToString();
                                    }

                                    strService = strService.Remove(0, 1);

                                    db.CreateProviderServices(Convert.ToInt32(sp.spId), strService);
                                }
                            }

                            // User Creation
                            string strPW = CreateRandomPassword(8);
                            User user = new User();
                            user.usUserName = model.ProviderPhone;
                            user.usPassword = strPW;
                            user.usIsActive = true;
                            user.usIsNewMember = true;
                            user.usType = EventManagement.Models.Enumerator.UserType.ServiceProvider.ToString();

                            db.Users.AddObject(user);
                            db.SaveChanges();

                            if (user.usId > 0)
                            {
                                var original = db.ServiceProviders.Where(t => t.spId == sp.spId).FirstOrDefault();
                                original.spUserId = user.usId;
                                db.ServiceProviders.ApplyCurrentValues(original);
                                db.SaveChanges();
                            }



                            //var dd = Request.Files.Count;
                            //var tt = Request.ContentLength;
                            ////ImageUpload iu = new ImageUpload();


                            

                            foreach (string upload in Request.Files)
                            {
                                if (!HasFile(Request.Files[upload])) continue;

                                string mimeType = Request.Files[upload].ContentType;
                                Stream fileStream = Request.Files[upload].InputStream;
                                string fileName = Path.GetFileName(Request.Files[upload].FileName);
                                int fileLength = Request.Files[upload].ContentLength;
                                byte[] fileData = new byte[fileLength];//image data type entry
                                fileStream.Read(fileData, 0, fileLength);
                                ImageUpload iu = new ImageUpload();
                                iu.Image = fileData;
                                iu.ImgName = fileName;
                                iu.ImgMime = mimeType;
                                iu.ImgUploaderId = sp.spId;

                                db.ImageUploads.AddObject(iu);
                                db.SaveChanges();
                            }


                        }

                        transaction.Complete();
                    }
                }

                return RedirectToAction("CreateService", "ServiceProvider");
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(ex.Source, ex.Message);
                return View("Error");
            }
        }

        public ActionResult EditService(int providerId)
        {
            try
            {
                ServicesViewModel vmServices = new ServicesViewModel();

                var events = from e in db.Events
                             select new
                             {
                                 EventId = e.evntID,
                                 EventName = e.evntName
                             };

                var religians = from r in db.Religions
                                select new
                                {
                                    ReligianId = r.relgID,
                                    ReligianName = r.relgName
                                };

                ViewBag.Events = events;
                ViewBag.ReligianList = religians;                

                var serviceProvider = from s in db.ServiceProviders
                                      where s.spId == providerId
                                      select new {                                       
                                          ProviderId = s.spId,
                                          Comapny = s.spCompanyName,
                                          Name = s.spName,
                                          Email = s.spEmail,
                                          Phone = s.spPhone,
                                          Comment = s.spComment,
                                          UploadImages = s.spUploadImage,
                                          Address = s.spAddress,
                                          Invite = s.spInvitees,
                                          ReligianId = s.spRelId
                                      
                                      };


                if (serviceProvider.Count() > 0 && serviceProvider != null)
                {
                    vmServices.ProviderId = serviceProvider.FirstOrDefault().ProviderId;
                    vmServices.ProviderCompany = serviceProvider.FirstOrDefault().Comapny;
                    vmServices.ProviderName = serviceProvider.FirstOrDefault().Name;
                    vmServices.ProviderEmail = serviceProvider.FirstOrDefault().Email;
                    vmServices.Comment = serviceProvider.FirstOrDefault().Comment;
                    vmServices.NoOfImages = Convert.ToInt32(serviceProvider.FirstOrDefault().UploadImages);
                    vmServices.ProviderPhone = serviceProvider.FirstOrDefault().Phone;
                    vmServices.ProviderAddress = serviceProvider.FirstOrDefault().Address;
                    vmServices.Invitees = serviceProvider.FirstOrDefault().Invite;
                    vmServices.ReligianId = Convert.ToInt32(serviceProvider.FirstOrDefault().ReligianId);

                    var providerService = from s in db.ProviderServices
                                          where s.spId == providerId
                                          select new { SericeId = s.svcId };

                    var providerEvent = from s in db.ProviderServices
                                        join es in db.EventServices
                                        on s.svcId equals es.svcID
                                        where s.spId == providerId
                                        select new { EventId = es.evntID };

                    string strService = string.Empty;
                    string strEvent = string.Empty;

                    foreach (var i in providerService)
                    {
                        //vmCustEvent.lstServices.Add(Convert.ToString(i.SericeId));
                        strService += "," + i.SericeId.ToString();
                    }
                    foreach (var i in providerEvent)
                    {
                        strEvent += "," + i.EventId.ToString();
                    }

                    //vmCustEvent.Selected = vmCustEvent.lstServices.ToArray();
                    if (!string.IsNullOrEmpty(strService) && !string.IsNullOrEmpty(strEvent))
                    {
                        vmServices.selectedServices = strService.Remove(0, 1);
                        vmServices.selectedEvents = strEvent.Remove(0, 1);
                    }

                    // Images
                }

                return View("EditService", vmServices);
            }
            catch (Exception ex)
            {
                
                ModelState.AddModelError(ex.Source, ex.Message);
                return View("Error");
            }
            
        }
        
        [HttpPost]
        public ActionResult EditService(ServicesViewModel model)
        {

            try
            {
                if (model.Selected != null)
                {
                    using (TransactionScope transaction = new TransactionScope())
                    {

                        using (var context = new EventManagementEntities())
                        {
                            ServiceProvider provider = new ServiceProvider();

                            var original = context.ServiceProviders.Where(t => t.spId == model.ProviderId).FirstOrDefault();

                            if (original != null)
                            {
                                original.spName = model.ProviderName;
                                original.spCompanyName = model.ProviderCompany;
                                original.spAddress = model.ProviderAddress;
                                original.spPhone = model.ProviderPhone;
                                original.spEmail = model.ProviderEmail;
                                original.spComment = model.Comment;
                                original.spUploadImage = model.NoOfImages;
                                original.spInvitees = model.Invitees;
                                original.spRelId = model.ReligianId;

                                context.ServiceProviders.ApplyCurrentValues(original);
                                context.SaveChanges();

                                string strService = string.Empty;

                                //// Service Insert bulk
                                //if (model.Selected != null)
                                //{
                                    if (model.Selected.Length > 0)
                                    {
                                        foreach (var item in model.Selected)
                                        {
                                            strService += "," + item.ToString();
                                        }

                                        strService = strService.Remove(0, 1);

                                        db.UpdateProviderServices(Convert.ToInt32(model.ProviderId), strService);
                                    }
                                //}

                                var dd = Request.Files.Count;
                                var tt = Request.ContentLength;
                                ////ImageUpload iu = new ImageUpload();

                                int count = 0;

                                if (dd > 0)
                                {
                                    //context.DeleteOldImages(Convert.ToInt32(model.ProviderId));

                                    foreach (string upload in Request.Files)
                                    {
                                        if (!HasFile(Request.Files[upload])) continue;
                                        count = count + 1;
                                        string mimeType = Request.Files[upload].ContentType;
                                        Stream fileStream = Request.Files[upload].InputStream;
                                        string fileName = Path.GetFileName(Request.Files[upload].FileName);
                                        int fileLength = Request.Files[upload].ContentLength;
                                        byte[] fileData = new byte[fileLength];//image data type entry
                                        fileStream.Read(fileData, 0, fileLength);

                                        if (count == 1)
                                        {
                                            context.DeleteOldImages(model.ProviderId);
                                        }

                                        ImageUpload iu = new ImageUpload();
                                        iu.Image = fileData;
                                        iu.ImgName = fileName;
                                        iu.ImgMime = mimeType;
                                        iu.ImgUploaderId = model.ProviderId; ;

                                        context.ImageUploads.AddObject(iu);
                                        context.SaveChanges();
                                        //count = count + 1;
                                    }
                                }
                            }

                        }
                    }
                }
                return RedirectToAction("GetAllServiceProvider", "ServiceProvider");
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(ex.Source, ex.Message);
                return View("Error");
            }
        }

        public ActionResult DeleteService(int providerId)
        {
            try
            {
                if (providerId > 0)
                {
                    using (TransactionScope transaction = new TransactionScope())
                    {
                        db.DeleteServiceProvider(Convert.ToInt32(providerId));

                        transaction.Complete();
                    }
                }
                return RedirectToAction("GetAllServiceProvider", "ServiceProvider");
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(ex.Source, ex.Message);
                return View("Error");
            }
            
        }


        #region Page Method

        private bool HasFile(HttpPostedFileBase file)
        {
            return (file != null && file.ContentLength > 0) ? true : false;
        }

        private static string CreateRandomPassword(int PasswordLength)
        {
            string _allowedChars = "0123456789abcdefghijkmnopqrstuvwxyzABCDEFGHJKLMNOPQRSTUVWXYZ";
            Random randNum = new Random();
            char[] chars = new char[PasswordLength];
            int allowedCharCount = _allowedChars.Length;
            for (int i = 0; i < PasswordLength; i++)
            {
                chars[i] = _allowedChars[(int)((_allowedChars.Length) * randNum.NextDouble())];
            }
            return new string(chars);
        } 

        #endregion
    }
}

