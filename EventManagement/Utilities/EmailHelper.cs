﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net.Mail;
using System.Text;
using UserModel = EventManagement.Models.UserViewModel;

namespace EventManagement.Utilities
{
    public class EmailHelper
    {
        public string _From, _To;
        public int _UserID;

        public EmailHelper(string To, int UserID)
        {
            _From = System.Configuration.ConfigurationManager.AppSettings["EmailFrom"];
            _To = To;
            _UserID = UserID;
        }

        private UserModel GetUserDetails()
        {
            EventManagementEntities db = new EventManagementEntities();
            UserModel user = (from u in db.Users
                              join c in db.Customers on u.usId equals c.custID                             
                              where u.usId == _UserID
                              && u.usIsActive == true
                              && u.usIsNewMember == true
                              select new UserModel
                              {
                                  FullName = c.custName,
                                  Password = u.usPassword,
                                  UserName = u.usUserName,
                                  UserType = u.usType

                              }).FirstOrDefault();

            return user;
        }

        public void SendMail()
        {
            UserModel user = GetUserDetails();            
            MailMessage mail = new MailMessage(_From, _To);
            SmtpClient client = new SmtpClient(System.Configuration.ConfigurationManager.AppSettings["EmailHost"]);
            StringBuilder mailBody = new StringBuilder();

            mailBody.AppendFormat("<h1>Heading Here</h1>");
            mailBody.AppendFormat("Dear {0}", user.FullName);
            mailBody.AppendFormat("<br />");
            mailBody.AppendFormat("<p>Thank you for registering in AlaEvent.</p>");
            mailBody.AppendFormat("<br />");
            mailBody.AppendFormat("<p>We have auto generated your username and password which are provided below.");
            mailBody.Append("Please activate your account <a href=\"http://www.wishmeal.com/EventManagement/Home/Login\">here</a></p>");
            mailBody.AppendFormat("<br />");
            mailBody.AppendFormat("Username: {0}", user.UserName);
            mailBody.AppendFormat("<br />");
            mailBody.AppendFormat("Password: {0}", user.Password);
            mailBody.AppendFormat("<br />");
            mailBody.AppendFormat("<br />");
            mailBody.AppendFormat("<br />");
            mailBody.AppendFormat("Thanks & Regards");
            mailBody.AppendFormat("<br />");
            mailBody.AppendFormat("AlaEvent");
            
            client.DeliveryMethod = SmtpDeliveryMethod.Network;
            client.Credentials = new System.Net.NetworkCredential
                                    (
                                        System.Configuration.ConfigurationManager.AppSettings["EmailUserName"],
                                        System.Configuration.ConfigurationManager.AppSettings["EmailPassword"]
                                    );            
            mail.Subject = "Welcome to AlaEvent";
            mail.IsBodyHtml = true;
            mail.Body = mailBody.ToString();
            client.Send(mail);
        }
    }
}